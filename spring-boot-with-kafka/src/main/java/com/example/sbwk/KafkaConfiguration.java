package com.example.sbwk;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
@EnableKafka
public class KafkaConfiguration {

    public static final String EXAMPLE_STRING_TOPIC = "ExampleStringTopic";
    public static final String EXAMPLE_DTO_TOPIC = "ExampleDtoTopic";

    @Bean
    public NewTopic exampleStringTopic() {
        return TopicBuilder.name(EXAMPLE_STRING_TOPIC).partitions(10).replicas(1).build();
    }

    @Bean
    public NewTopic exampleDtoTopic() {
        return TopicBuilder.name(EXAMPLE_DTO_TOPIC).partitions(2).replicas(1).build();
    }
}
