package com.example.sbwk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbwkApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbwkApplication.class, args);
	}

}
