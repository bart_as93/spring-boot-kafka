package com.example.sbwk;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ExampleKafkaListener {

    @KafkaListener(id = "kafkaStringListener012345", topics = KafkaConfiguration.EXAMPLE_STRING_TOPIC,
            topicPartitions = @TopicPartition(topic = KafkaConfiguration.EXAMPLE_STRING_TOPIC,
                    partitions = {"0", "1", "2", "3", "4", "5"}))
    public void kafkaStringListenerPartition12345(String message) {
        log.debug("[0,1,2,3,4,5] Listened: " + message);
    }

    @KafkaListener(id = "kafkaStringListener6789", topics = KafkaConfiguration.EXAMPLE_STRING_TOPIC,
            topicPartitions = @TopicPartition(topic = KafkaConfiguration.EXAMPLE_STRING_TOPIC, partitions = {"6", "7", "8", "9"}))
    public void kafkaStringListener6789(String message) {
        log.debug("[6,7,8,9] Listened: " + message);
    }

    @KafkaListener(id = "kafkaDtoListener", topics = KafkaConfiguration.EXAMPLE_DTO_TOPIC)
    public void kafkaDtoListener(ExampleDto message) {
        log.debug("Listened dto: " + message);
    }
}
