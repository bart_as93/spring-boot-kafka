package com.example.sbwk;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

@RequiredArgsConstructor
@RestController
@Slf4j
public class ExampleStringController {

    private final KafkaTemplate<String, Object> kafkaTemplate;
//    private final Set<String> messages = new ConcurrentSkipListSet<>();

    @GetMapping("/test/string/produce")
    public void stringProducer(@RequestParam(value = "batchSize", required = false, defaultValue = "1000") Integer batchSize) {
        IntStream.range(0, batchSize)
                .forEach(i -> {
                    CompletableFuture<SendResult<String, Object>> future = kafkaTemplate.send(KafkaConfiguration.EXAMPLE_STRING_TOPIC, String.valueOf(i), "Message: " + i);
                    future.whenComplete((result, ex) -> {
                        if (ex == null) {
                            log.debug("Sent message: " + i + " res: " + result);
                        } else {
                            log.debug("Unable to send " + i + " message due to : " + ex.getMessage());
                        }
                    });
                });
    }
}
