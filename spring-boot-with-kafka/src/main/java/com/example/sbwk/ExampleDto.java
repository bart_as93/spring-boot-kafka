package com.example.sbwk;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ExampleDto implements Serializable {
    private String id;
    private String name;
    private BigDecimal price;

    public static ExampleDto createRandom() {
        ExampleDto dto = new ExampleDto();
        int randomValue = (int) (Math.random() * 1000);
        dto.setId("id-" + randomValue);
        dto.setName("Name: " + randomValue);
        dto.setPrice(BigDecimal.TEN);
        return dto;
    }
}
