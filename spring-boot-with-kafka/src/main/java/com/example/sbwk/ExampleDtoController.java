package com.example.sbwk;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

@RequiredArgsConstructor
@RestController
@Slf4j
public class ExampleDtoController {

    private final KafkaTemplate<String, ExampleDto> kafkaTemplate;

    @GetMapping("/test/dto/produce")
    public void dtoProducer(@RequestParam(value = "batchSize", required = false, defaultValue = "1000") Integer batchSize) {
        IntStream.range(0, batchSize)
                .forEach(i -> {
                    ExampleDto message = ExampleDto.createRandom();
                    CompletableFuture<SendResult<String, ExampleDto>> future = kafkaTemplate.send(KafkaConfiguration.EXAMPLE_DTO_TOPIC, String.valueOf(i), message);
                    future.whenComplete((result, ex) -> {
                        if (ex == null) {
                            log.debug("Sent " + i + " (messageId: " + message.getId() + ") message=[" + message +
                                    "] with offset=[" + result.getRecordMetadata().offset() + "]");
                        } else {
                            log.debug("Unable to send " + i + " (messageId: " + message.getId() + ") message=[" +
                                    message + "] due to : " + ex.getMessage());
                        }
                    });
                });
    }
}
