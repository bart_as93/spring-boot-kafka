# Spring boot application with kafka
1. To run apache kafka with zookeeper and kafka-ui run
`
cd kafka
docker-compose up
`
2. Run http://localhost:8080 to view kafka ui
3. Run SkwkApplication (spring-boot).
4. Run curl
   curl http://localhost:8090/test/string/produce?batchSize=1000
   curl http://localhost:8090/test/dto/produce?batchSize=1000